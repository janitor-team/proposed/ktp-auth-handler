Source: ktp-auth-handler
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Diane Trout <diane@ghic.org>,
           Michał Zając <quintasan@kubuntu.org>,
           Norbert Preining <norbert@preining.info>,
Build-Depends: cmake (>= 2.8.12~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 0.0.11~),
               gettext,
               libaccounts-qt5-dev (>= 1.10),
               libkaccounts-dev,
               libkf5i18n-dev (>= 5.0~),
               libkf5kio-dev (>= 5.0~),
               libkf5wallet-dev (>= 5.0~),
               libkf5webkit-dev (>= 5.0~),
               libkf5widgetsaddons-dev (>= 5.0~),
               libktp-dev (>= 21.04.0~),
               libqca-qt5-2-dev,
               libsignon-qt5-dev (>= 8.55~),
               libtelepathy-logger-qt-dev (>= 17.08~),
               libtelepathy-qt5-dev (>= 0.9.7~),
               pkg-kde-tools (>= 0.9),
               qtbase5-dev (>= 5.4),
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://projects.kde.org/projects/kde/kdenetwork/ktp-auth-handler
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/ktp-auth-handler.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/ktp-auth-handler

Package: kde-telepathy-auth-handler
Architecture: any
Depends: kde-telepathy-data (>= ${source:Upstream-Version}),
         libqca-qt5-2-plugins,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: kde-telepathy
Description: KDE Telepathy authentication handler
 This package provides a Telepathy handler for the ServerAuthentication
 and ServerTLSConnection Telepathy channel types, which means that it
 provides passwords to connection managers (and stores them using KWallet)
 and also handles SSL certificate errors in encrypted IM connections.
 .
 This package is not meant to be used standalone. It is recommended
 to install the kde-telepathy metapackage instead.
